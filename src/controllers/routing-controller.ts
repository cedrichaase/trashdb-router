import { MetadataRepository } from "../repositories/metadata-repository";
import { MuleService } from "../services/mule-service";

export class RoutingController {

    private metadataRepository: MetadataRepository;
    private muleService: MuleService;

    constructor({metadataRepository, muleService}) {
        this.metadataRepository = metadataRepository;
        this.muleService = muleService;
    }

    public insert(collection, document) {
        const shardKey = this.metadataRepository.getShardKey(collection);

        const shardKeyValue = document[shardKey];
        if (!shardKeyValue) {
            throw new Error(`Document ${JSON.stringify(document)} does not contain shard key ${shardKey}`);
        }

        const muleId = this.metadataRepository.getMuleIdByShardKeyValue(collection, shardKeyValue);

        const insertedDocument = this.muleService.insert(collection, muleId, document);

        console.log(insertedDocument);

        return insertedDocument;
    }

    public find(collection, criteria) {
        const muleIds = this.metadataRepository.getMuleIdsForCriteria(collection, criteria);

        const resultsByMule = muleIds.map(mid => this.muleService.find(collection, mid, criteria));

        return [].concat.apply([], resultsByMule);
    }
}