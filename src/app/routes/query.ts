import { QueryAction } from "../../actions/query-action";

module.exports = app => {
    app.use((req, res, next) => {
        const action: QueryAction = req.scope.resolve("queryAction");
        req.queryAction = action;
        return next();
    });

    // app.get("/collections", (req, res) => {
    //     return req.collectionAction.list(req, res);
    // });
    //
    app.get("/collections/:name", (req, res) => {
        return req.queryAction.find(req, res);
    });

    app.post("/collections/:name", (req, res) => {
        return req.queryAction.insert(req, res);
    });

    // app.patch("/collections/:name", (req, res) => {
    //     return req.collectionAction.update(req, res);
    // });
    //
    // app.delete("/collections/:name", (req, res) => {
    //     return req.collectionAction.delete(req, res);
    // });
};

