import { QueryAction } from "../../actions/query-action";
import { RoutingController } from "../../controllers/routing-controller";
import { MuleService } from "../../services/mule-service";
import { MetadataRepository } from "../../repositories/metadata-repository";

const path = require("path");
const awilix = require("awilix");
const { createContainer, asClass, asValue } = awilix;

const container = createContainer();

// environment
const cliOptions = require("minimist")(process.argv.slice(2));
const environment = Object.assign({
    port: 3000,
    db: "/usr/local/var/trashdb",
    shard: "0"
}, cliOptions);

environment.db = path.resolve(environment.db);

container.register({
    environment: asValue(environment)
});


// services
container.register({
    queryAction: asClass(QueryAction).scoped()
});
container.register({
    routingController: asClass(RoutingController).scoped()
});
container.register({
    muleService: asClass(MuleService).scoped()
});
container.register({
    metadataRepository: asClass(MetadataRepository).scoped()
});

export default container;
