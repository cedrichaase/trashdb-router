const collections = [
    {
        name: "users",
        shardKey: "origin",
        shards: [
            {
                prefix: "1000011000001111010000100110110010111010101001100100100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                host: "http://localhost:3002"
            },
            {
                prefix: "1",
                host: "http://localhost:3003"
            },
            {
                prefix: "01",
                host: "http://localhost:3004"
            },
            {
                prefix: "00",
                host: "http://localhost:3005"
            }
        ]
    }
];

const leftpad = (str, ans) => {
    const pad = "0".repeat(ans);
    return pad.substring(0, pad.length - str.length) + str;
};

const hex2bin = (hex) => {
    return leftpad(parseInt(hex, 16).toString(2), 160);
};

const sha1 = (input) => {
    const sha1hex = require("crypto")
        .createHash("sha1")
        .update(JSON.stringify(input))
        .digest("hex");

    return hex2bin(sha1hex);
};


export class MetadataRepository {
    constructor() {

    }

    private getMuleIdByBinaryShardKeyHash(collection, hash) {
        const data = this.findMetaDataByCollectionName(collection);

        // mule ID is the longest shard key value prefix found in shards for the collection
        let longestPrefix = "";

        for (const shard of data.shards) {
            if (!hash.startsWith(shard.prefix)) { continue; }

            if (shard.prefix.length > longestPrefix.length) {
                longestPrefix = shard.prefix;
                console.log(longestPrefix, longestPrefix.length);
            }
        }

        if (longestPrefix.length < 1) {
            throw Error("No common prefix found!");
        }

        return longestPrefix;
    }

    public getMuleIdByShardKeyValue(collection, shardKeyValue: string) {
        return this.getMuleIdByBinaryShardKeyHash(collection, sha1(shardKeyValue));
    }

    public getMuleIdByDocumentId(collection, documentId: string) {
        const shardKeyHex = documentId.split(":")[0];
        return this.getMuleIdByBinaryShardKeyHash(collection, hex2bin(shardKeyHex));
    }

    public getMuleIdsForCriteria(collection, criteria) {
        const id = criteria["id"];
        if (id) {
            return [this.getMuleIdByDocumentId(collection, id)];
        }

        const shardKey = this.getShardKey(collection);
        const shardKeyValue = criteria[shardKey];
        if (shardKeyValue) {
            return [this.getMuleIdByShardKeyValue(collection, shardKeyValue)];
        }

        const meta = this.findMetaDataByCollectionName(collection);
        return meta.shards.map(shard => shard.prefix);
    }

    public getHostByMuleId(collection, muleId) {
        const data = this.findMetaDataByCollectionName(collection);

        const shard = data.shards.find(shard => shard.prefix === muleId);

        if (!shard) {
            throw new Error(`No shard found for mule ID ${muleId}`);
        }

        return shard.host;
    }

    public getShardKey(collection) {
        const metaData = this.findMetaDataByCollectionName(collection);
        return metaData.shardKey;
    }

    private findMetaDataByCollectionName(name) {
        const collection = collections.find(c => c.name === name);

        if (!collection) {
            throw new Error(`No metadata found for collection ${name}`);
        }

        return collection;
    }
}