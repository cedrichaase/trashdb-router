import { MetadataRepository } from "../repositories/metadata-repository";
const rp = require("request-promise");

export class MuleService {
    private metadataRepository: MetadataRepository;

    constructor({metadataRepository}) {
        this.metadataRepository = metadataRepository;
    }

    public async insert(collection, muleId, document) {
        const host = this.metadataRepository.getHostByMuleId(collection, muleId);

        const options = {
            method: "POST",
            uri: `${host}/collections/${collection}`,
            body: { document },
            json: true
        };

        const response = (await rp(options));

        return response.document;
    }

    public async find(collection, muleId, criteria) {
        const host = this.metadataRepository.getHostByMuleId(collection, muleId);

        const options = {
            method: "GET",
            uri: `${host}/collections/${collection}`,
            body: { criteria },
            json: true
        };

        return (await rp(options)).document;
    }
}