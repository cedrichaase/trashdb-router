import { RoutingController } from "../controllers/routing-controller";

export class QueryAction {
    private routingController: RoutingController;

    constructor({routingController}) {
        this.routingController = routingController;
    }

    public async insert(req, res) {
        const collection = req.params["name"];
        const document = req.body["document"];

        try {
            const insertedDocument = await this.routingController.insert(collection, document);
            console.log(insertedDocument);
            return res.status(201).send({document: insertedDocument});
        } catch (error) {
            console.error(error);
            return res.status(500).send({error});
        }
    }

    public async find(req, res) {
        const collectionName = req.params["name"];
        const criteria = req.body["criteria"];

        const documents = await Promise.all(this.routingController.find(collectionName, criteria));

        console.log(documents);

        return res.status(200).send({documents});
    }
}