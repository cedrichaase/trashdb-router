# Express Template

A routing service for [TrashDB](https://gitlab.com/cedrichaase/trashdb).
In the TrashDB infrastructure, the TrashDB router is the single source of truth
for which range of shard key values should be managed by which host.
For now, this is information is [hard coded](https://gitlab.com/cedrichaase/trashdb-router/blob/master/src/repositories/metadata-repository.ts).

## Setup

```bash
$ npm install -g grunt-cli
$ npm install
```

## Development

During development, use `npm run watch` to keep transpiling,
linting and restarting your express app whenever a file is saved.

Before committing, use `npm run precommit` to check if your code builds
and to iron out any tslint errors

Use `npm run ci` to do a clean npm install as well as build, lint
and run tests for your application.
